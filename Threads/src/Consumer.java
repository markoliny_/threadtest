public class Consumer extends Thread implements Runnable {
    private Box box;

    Consumer(Box b) {
        box = b;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i <= 30; i++) {
                if (!isInterrupted()) {
                    sleep(150);
                    System.out.println("Consumer got: " + box.get());
                } else {
                    throw new InterruptedException();
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Thread is interrupted");
        }
    }
}
