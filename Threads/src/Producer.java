public class Producer extends Thread implements Runnable {
    private Box box;

    Producer(Box b) {
        box = b;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i <= 30; i++) {
                if (!isInterrupted()) {
                    box.put(i);
                    sleep(100);
                    System.out.println("Producer put: " + i);
                } else {
                    throw new InterruptedException();
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Thread is interrupted");
        }
    }
}
