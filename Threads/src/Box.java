public class Box {
    private volatile int value;
    private volatile boolean available = false;

    synchronized int get(){
        while (!available)
            try {
                wait();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        available = false;
        notify();
        return value;
    }

    synchronized void put(int num){
        while (available) {
            try {
                wait();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        value = num;
        available = true;
        notify();
    }

    public static void main(String[] args) {
        Box box = new Box();
        new Producer(box).start();
        new Consumer(box).start();
    }
}
/* Задание:
   Есть класс Box у него есть поле с типом int value, и флаг available который говорит,
 что значение заполнено. Есть два других класса реализующих интерфейс Runnable: Producer и Consumer.
 Идея такая: Классу Producer нужно положить в ящик подряд 30 чисел от 0 до 29, а Consumer прочитать из ящика 30 чисел.
 */